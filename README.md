## JS-10 - React example project - Animations
This is an example React project covering multiple React features. Based on UDEMY course: https://www.udemy.com/react-the-complete-guide-incl-redux/.

**Examples in this project are focused mainly on Animations.**

#### Used commands and libs:
* npm install react-transition-group --save
